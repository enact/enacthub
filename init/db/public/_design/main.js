by_app_map = function (doc){
    emit(doc.app, doc)
}

by_type_map = function (doc){
    emit(doc.type, doc)
}

let doc = {
    views:{
        by_app:{
            map: by_app_map.toString()
        },
        by_type:{
            map: by_type_map.toString()
        }
    }
}

module.exports = doc